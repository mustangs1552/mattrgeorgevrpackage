﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;
using MattRGeorge.Editor.Versioning;

namespace Assets.Editor
{
    public class ApplyVersionToMattRGeorgeVR : ApplyVersionToPackage
    {
        public static string packagePath = @"\MattRGeorgeVR";

        [MenuItem("Versioning/Apply Git Version to MattRGeorgeVR Package")]
        [InitializeOnLoadMethod]
        public static void ApplyGitVersionToMattRGeorgePackage()
        {
            string packagesFolder = Application.dataPath.Replace("/Assets", @"/Packages");
            ApplyGitVersionToPackage(packagesFolder.Replace("/", @"\") + ((packagePath[0] == '\\' || packagePath[0] == '/') ? "" : @"\") + packagePath);
        }

        public override void OnPreprocessBuild(BuildReport report)
        {
            ApplyGitVersionToMattRGeorgePackage();
        }
    }
}
