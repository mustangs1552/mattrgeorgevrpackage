# Changelog

Format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
Project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

To see pre-NPM hosted changelog look at: preNPMHosted_CHANGELOG.md.

## [5.0.0] - 4/5/20
### [Added]
- ThrowableWeapon base class with basic throwable behaviour.
- New sample assets for FPS controller and sample weapons and related items.
### [Changed]
- Inventory system to use actual objects rather than counts.
- Ammo functionality to work better and support storing and usage of both raw ammo and ammo in packs.
- Inventory.allowDuplicateSlots is now allowNewDuplicateSLots and is now public.

## [4.0.0] - 3/19/20
### [Added]
- Ability to drop a weapon from the WeaponManager by Weapon.
- Pickup/Drop events for WeaponManager when weapons are pickedup/dropped.
### [Changed]
- How Weapon pickingup/dropping works. WeaponManager now simply calls the Pickup/Drop methods on the Weapon and the Weapon calls what it needs to. This now supports pickingup/dropping weapons from WeaponManager or Weapon.
- Weapon.SetPickedupState() to protected from public.
### [Removed]
- WeaponManager.CheckCanDropWeapon().

## [3.0.1] - 3/18/20
### [Added]
- ApplyForceToObject component that behaves similarly to TranslateObject.
- UnityLoggingUtility.LogMissingValue() that posts an error message to inform user of a missing required value.

## [3.0.0] - 3/17/20
### [Added]
- New base AssetLibrary class that has a list of named object values and list of named object lists.
- New NameListGameObject general helper class for named lists of objects and a to dictionary method
- A to dictionary method to NameGameObject.
### [Changed]
- Locations of PickupableItem and UsableItem from InventorySystem to Player in both namespace and file location.
- Name of PlayerControls to PlayerManager and now inherits from the new AssetLibrary base class.
- Name of previous AssetLibrary to AssetLibraryWithChances which inherites from new AssetLibrary base class.
- IUsable Use() method parameter is now PlayerManager instead of GameObject.
- Projectile user is now PlayerManager instead of GameObject.
- Weapon now uses PlayerManager's inherited AssetLibrary to find desired WeaponManager allowing the ability to have multiple WeaponManagers managing different types of weapons.
### [Removed]
- Second and third parameters of TakeDamage() and Heal() for ITakesDamage.
### [Fixed]
- Discovered bug where the source object and source user of Projectile.OnSpawn() was flipped when calling it from ShootableWeapon.

## [2.0.3] - 3/14/20
### [Added]
- The ability to select no weapon to WeaponManager.

## [2.0.2] - 3/13/20
### [Added]
- The ability to not allow more than one slot with the same item in Inventory.
- Ability to switch to previously selected weapon.
### [Fixed]
- Some discovered bugs related to switching to a weapon that is the same.

## [2.0.1] - 3/12/20
### [Added]
- The ability for the player to pickup/drop ammo via a new AmmoPack.
### [Fixed]
- Bug where weapons were not being dropped.

## [2.0.0] - 3/11/20
### [Added]
- Backtracing to CollisionDetector to improve collision detection on fast moving objects that may have skipped collisions.
- TranslateObjectViaNodes off of TranslateObject that simply adds ability to move an object between nodes in the world.
- DestroyObject that has a destroy method that can be called from an inspector event and can be toggled between Unity destroy and object pool destroy.
- ConversionUtility() with methods that can convert and/or check a string to a primitive type.
- JSONObject.ToJSON() to convert it to JSON text.
- Auto versioning tools using git tags and commits to generate a version and apply it to the project version and can also apply it to a package.json.
- A health stat system.
- PickupableItem and UsableItem which inherites from Item.
- A weapon system.
- TranslateObject which simply translates an object down its z-axis.
- Started adding ToJSON() support for JSONParser.
### [Changed]
- Folder name and namespace of MattRGeorge.General.Misc to .Tools.
- Folder name and namespace of MattRGeorge.Unity.Tools.Misc to .Stats.
- Location of MattRGeorge.General.Enums.GenericEnumerators to MattRGeorge.General.Utilities.
- Location of MattRGeorge.General.Utilities.ReflectionUtility to MattRGeorge.General.Utilities.Static.
- Item.ItemName now returns actual name without any "(clone)"s.
### [Removed]
- MattRGeorge.General.Enums
### [Fixed]
- Couple bugs with Inventory.

## [1.4.2] - 3/7/20
### [Changed]
- Added option to allow spawning more than one objects at the same node to ObjectSpawner and ObjectSpawnerViaAssetLibrary.
- Added OnSpawnFailed() to SpawnObjectBase that can be overrided and is called when the Spawn() coroutine fails to spawn an object.
- Added pending objects count to SpawnObjectBase so you can tell if there are objects still waiting to be spawned in the spawn coroutine.

## [1.4.1] - 3/7/20
### [Changed]
- Inheritance design of SpawnObject family. SpawnObjects and ObjectSpawner inheritance level are now responsible for max counts instead of auto versions.

## [1.4.0] - 3/6/20
### [Added]
- ObjectSpawnerViaAssetLibrary and an auto version with total count limit and per list name count limit.
- Base abstract classes for SpawnObject and ObjectSpawner.
- New methods to AssetLibrary that allows returning the list name that was used.
### [Changed]
- ObjectSpawnerAuto.OnValidate() to protected virtual.
- AssetLibrary list names are now case sensitive.
- Renamed SpawnObjectsAuto.Spawn() to SpawnObject() to match other classes.
### [Fixed]
- Fixed a bug where some SpawnObject classes were not allowing the unlimited option for maxCount.

## [1.3.0] - 3/4/20
### [Added]
- An ObjectSpawner and an auto version which are node based spawners using SpawnObject as the node.
- MIT License.
- New method to ListUtility that can work with different modes for selecting objects from a list and is able to continue past the limits of the invalid indexes list.
- SpawnObject that is now the base class of all SpawnObject classes.
- A logger class that simply allows for calling Debug.Log...() methods from inspector events.
### [Changed]
- SpawnObject family of classes inheritance design.
### [Removed]
- ObjectPool prefab from samples.

## [1.2.2] - 3/1/20
### [Added]
- Static method to convert List<GameObjectWithChance> to List<ObjectWithChance<GameObject>> on GameObjectWithChanceList.

## [1.2.1] - 3/1/20
### [Changed]
- AssetLibrary to better support inheritance.
- Moved private methods for converting GameObjectWithChance (not static) and GameObjectWithChanceList (static) to thier respected helper classes.

## [1.2.0] - 3/1/20
### [Added]
- ListUtility.RemoveNullEntries() which removes all null values from the given list.
- A new family of classes for SpawnObject (SpawnObjects, SpawnObjectAuto (previously SpawnObject), SpawnObjectsAuto, and SpawnObjectsViaAssetLibrary).
- New method to AssetLibrary to include more than one list of objects when choosing an object.
### [Changed]
- MattRGeorge.Unity.Utilities.Components to .Utilities.Static and MattRGeorge.Unity.Utilities.Actions to .Utilities.Components.
- UITextMatcher and UIRatioUpdater to MattRGeorge.Unity.Utilities.Components.
- UIUtilities to MattRGeorge.Unity.Utilities.Static.
- UITextMatcher now using UnityEvent instead of Action for its event.
- Name of GameObjectUtilities and UIUtilities to ...Utility to match the rest.
- SpawnObject design to support multiple kinds of SpawnObject classes (Original is now SpawnObjectAuto).

## [1.1.0] - 2/29/20
### [Added]
- The AssetLibrary to keep lists of assets used by other objects and can be accessed randomly.
- A RaycasterDetector that can check for a raycast result either manually or automatically.
- OnValidate() to all necessary objects.
### [Changed]
- Location of helper classes to HelperClasses.
- All classes to follow new coding habits.

## [1.0.1] - 2/28/20
### [Added]
- Second changelog with old versions from before NPM hosted release.

## [1.0.0] - 2/28/20
- Initial NPM release.