﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorgeVR.Utilities.Static;

namespace MattRGeorgeVR.Tools.ArenaScale
{
    public class RoomExitArea : MonoBehaviour
    {
        [SerializeField] protected GameObject waypointObj = null;
        [SerializeField] protected RoomExitArea connectingRoomExitArea = null;
        [SerializeField] protected Collider roomArea = null;

        public virtual void EnteredArea(GameObject obj, Collider otherCol)
        {

        }

        private void MovePlayespace()
        {
            List<Vector3> boundryPoints = XRDeviceUtitlies.GetPlaySpaceBoundryPoints();
        }

        protected virtual void CheckMissingValues()
        {
            if (connectingRoomExitArea == null) UnityLoggingUtility.LogMissingValue(GetType(), "connectingRoomExitArea", gameObject);
            if (roomArea == null) UnityLoggingUtility.LogMissingValue(GetType(), "roomArea", gameObject);
        }

        protected virtual void Awake()
        {
            CheckMissingValues();
            
            MovePlayespace();
        }
    }
}
