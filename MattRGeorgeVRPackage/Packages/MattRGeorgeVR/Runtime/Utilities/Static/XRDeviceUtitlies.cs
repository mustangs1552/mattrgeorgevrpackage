﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace MattRGeorgeVR.Utilities.Static
{
    public static class XRDeviceUtitlies
    {
        public static InputDevice GetHMD()
        {
            List<InputDevice> inputDevices = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.HeadMounted | InputDeviceCharacteristics.Camera, inputDevices);
            return (inputDevices.Count > 0) ? inputDevices[0] : new InputDevice();
        }

        public static List<Vector3> GetPlaySpaceBoundryPoints()
        {
            InputDevice hmd = GetHMD();
            if (!hmd.isValid) return new List<Vector3>();

            List<Vector3> points = new List<Vector3>();
            hmd.subsystem.TryGetBoundaryPoints(points);
            return points;
        }
    }
}
